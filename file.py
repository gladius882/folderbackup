import os
import os.path
import shutil
import zipfile
import tarfile
import zlib
import ftplib
from tkinter import *
from tkinter import filedialog

class Source():
    """ Klasa reprezentujaca sciezke """
    def __init__(self, dir = "None"):
        self.__directory = dir
        self.file = False           # Czy to jest plik
        self.dir = False            # Czy to jest katalog
        self.exist = True           # Czy plik istnieje
        self.error_report = True    # Sposob raportowania bledow
        self.list = []              # Lista plikow w sciezce
        self.dirlist = []           # Lista katalogow w sciezce
    def __str__(self):
        ret = self.__directory
        return ret


    def ChangeErrorReport(self):
        self.error_report = not self.error_report

    def setDirectory(self, directory):
        if directory != "":
            self.__directory = directory
            self.LoadDirList()
            if os.path.isfile(self.__directory):
                self.file = True
            elif os.path.isdir(self.__directory):
                self.dir = True
            else:
                self.exist = False
        elif self.error_report == True:
            print("Nie podano prawidlowej sciezki")
    def getmtime(self):
        now = None
    def getDirectory(self):
        return self.__directory

    def getTail(self):
        if self.__directory not in ("None", ""):
            head, tail = os.path.split(self.__directory)
            return tail
        elif self.error_report == True:
            print("Nie podano sciezki")

    def LoadDirList(self):
        ListDir(self.__directory, self.list, self.dirlist)
        for i in self.dirlist:
            i.replace(self.__directory, "")

    def getList(self):
        self.LoadDirList()
        if len(self.list) > 0:
            return self.list
        else:
            return None


class Copy():
    """ Kopiuje zawartosc katalogu lub pojedynczy plik zaleznie od typu sciezki """
    def __init__(self, src, target, entry):
        self.src = src              # Sciezka zrodlowa
        self.target = target        # Sciezka docelowa
        self.options = {            # Slownik z opcjami
            "type" : "full",        # Mozliwe wartosci: full, incremental
            "time" : 7,             # Liczba
            "time-type" : "day"}    # Mozliwe wartosci: year ,month, day, hour, minute
        self.entry = entry

        self.src.LoadDirList()
        self.target.LoadDirList()
    def __str__(self):
        tmp = "Bedzie kopiowal z " + str(self.src) + " do "
        tmp += str(self.target) + "\nRodzaj kopii: " + self.options["type"]
        return tmp

    def setSrcDir(self, dir):
        if dir == "":
            print("Nie mozna przypisac pustego lancucha")
        else:
            if os.path.exists(dir):
                self.src = dir
            else:
                print("Podana sciezka nie istnieje")
    def setTargetDir(self, dir):
        if dir == "":
            print("Nie mozna przypisac pustego lancucha")
        else:
            if os.path.exists(dir):
                self.target = dir
            else:
                print("Podana sciezka nie istnieje")
    def __FullCopy(self):
        # Kopia calosciowa
        print("Kopia calosciowa")
        for i in self.src.dirlist:
            folder = i.replace(str(self.src), str(self.target))
            if not os.path.exists(folder):
                os.mkdir(folder)
                self.entry.insert(END, "Utworzono folder " + folder)

        self.src.list = list(set(self.src.list))
        #self.entry.insert(END, "Ilosc plikow do skopiowania: " + str(len(self.target.list)))
        for i in self.src.list:
            if os.path.isdir(i):
                file = i.replace(str(self.src), str(self.target))
            else:
                head, tail = os.path.split(i)
                file = self.target.getDirectory() + "/" + tail

            if os.path.exists(file):
                os.remove(file)
                self.entry.insert(END, "Usunieto " + file)

            #shutil.copy(i, file)
            os.system("copy \"" + i + "\" \"" + file + "\"")
            self.entry.insert(END, "Skopiowano " + i + " do " + file)
        return
    def __IncrementCopy(self):
        # Kopia przyrostowa
        print("Kopia przyrostowa")
        for i in self.src.dirlist:
            folder = i.replace(str(self.src), str(self.target))
            if not os.path.exists(folder):
                os.mkdir(folder)
                self.entry.insert(END, "Utworzono folder " + folder)

        #self.entry.insert(END, "Ilosc plikow do skopiowania: " + str(len(self.target.list)))
        for i in self.src.list:
            file = i.replace(str(self.src), str(self.target))
            if os.path.exists(file):
                t1 = os.path.getmtime(i)
                t2 = os.path.getmtime(file)
                if t1>t2:
                    os.remove(file)
                    shutil.copy(i, file)
                    self.entry.insert(END, "Skopiowano " + i + " do " + file + " -m")
            else:
                shutil.copy(i, file)
                self.entry.insert(END, "Skopiowano " + i + " do " + file + " -n")
        return
    def MakeCopy(self):
        if self.options["type"] == "full":
            self.__FullCopy()
        elif self.options["type"] == "incremental":
            self.__IncrementCopy()
        else:
            print("Podany typ kopi nie jest prawidlowy")

class Archiver():
    """ Klasa do tworzenia archiwum """
    def __init__(self, entry):
        self.entry = entry
        self.filelist = []          # Lista plikow do archiwum
        self.options = {            # Slownik z opcjami
            "type" : ".zip",        # Mozliwe wartosci: zip, tar, zlib
            "remove" : False,       # Czy usunac plik po spakowaniu
            "result" : "archive",   # Nazwa pliku archiwum
            "mode" : "w"}
    def addfile(self, filename):
        self.filelist.append(filename)
    def removefile(self, filename):
        self.filelist.remove(filename)
    def clear(self):
        del self.filelist[0:len(self.filelist)]
    def __zip(self):
        if len(self.filelist) == 0:
            print("Zanim utworzysz archiwum najpierw dodaj pliki")
        else:
            filename = self.options["result"] + self.options["type"]
            archive = zipfile.ZipFile(filename, self.options["mode"])

            self.entry.insert(END, "Plikow do zarchiwizowania: " + str(len(self.filelist)))
            for i in self.filelist:
                try:
                    archive.write(i)
                    self.entry.insert(END, "Dodano " + i + " do archiwum " + filename)
                except:
                    self.entry.insert(END, "Nie mozna dodac " + i + " do archiwum")

            archive.close()
    def __tar(self):
        if len(self.filelist) == 0:
            self.entry.insert(END, "Zanim utworzysz archiwum najpierw dodaj pliki")
        else:
            filename = self.options["result"] + self.options["type"]
            archive = tarfile.TarFile(filename, self.options["mode"])

            self.entry.insert(END, "Plikow do zarchiwizowania: " + str(len(self.filelist)))
            for i in self.filelist:
                try:
                    archive.add(i)
                    self.entry.insert(END, "Dodano " + i + " do archiwum " + filename)
                except:
                    self.entry.insert(END, "Nie mozna dodac " + i + " do archiwum")

            archive.close()
    def __zlib(self):
        if len(self.filelist) == 0:
            print("Zanim utworzysz archiwum najpierw dodaj pliki")
        else:
            filename = self.options["result"] + self.options["type"]
            archive = open(filename, self.options["type"])

            for i in self.filelist:
                try:
                    output = zlib.compress(i, zlib.Z_BEST_COMPRESSION)
                    archive.write(output)
                except:
                    print("Nie mozna dodac " + i + " do archiwum")


            archive.close()
    def deloldfiles(self):
        try:
            for i in self.filelist:
                if os.path.isdir(i):
                    shutil.rmtree(i)
                    print("Usunieto plik " + i)
                else:
                    os.path.isfile(i)
                    os.remove(i)
                    print("Usunieto folder " + i)
        except:
            print
    def create(self):
        if self.options["type"] == ".zip":
            self.__zip()
        elif self.options["type"] == ".tar":
            self.__tar()


class Backup(object):
    def __init__(self, date):
        self.target = None
        self.copy = []
        self.date = date



def ListDir(dir, list, dirlist):
    """

    """
    if os.path.isdir(dir):
        elements = os.listdir(dir)
    else:
        list.append(dir)
        return

    for el in elements:
        src = os.path.join(dir, el)
        if os.path.isdir(src):
            dirlist.append(src)
            ListDir(src, list, dirlist)
        else:
            list.append(src)


def getFolderSize(start_path = '.'):
    """ Zwraca rozmiar pliku w bajtach """
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            total_size += os.path.getsize(fp)
    return total_size


def askfile():
    """
    Wyswietla okno dialogowe do wyboru pliku
    i zwraca sciezke do niego
    """
    directory = filedialog.askopenfilename(initialdir="\\", title="Wskaz plik")
    if directory != "":
        return directory

def askfolder():
    """
    Wyswietla okno dialogowe do wyboru folderu
    i zwraca sciezke do niego
    """
    directory = filedialog.askdirectory(initialdir="\\", title="Wskaz folder")
    if directory != "":
        return directory

if __name__ == "__main__":
    print("File module")