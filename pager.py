from tkinter import *

class Pager(object):
    def __init__(self, root):
        self.frames = []
        self.active = 0
        self.x = 170
        self.y = 0
        self.width=550
        self.height=420
        self.root = root

    def add(self, frame):
        self.frames.append(frame)
    def update(self):
        if len(self.frames) == 0:
            print("No pages!")
        else:
            for i in self.frames:
                i.pack_forget()
                i.place_forget()
            self.frames[self.active].pack()
            self.frames[self.active].place(x=self.x,
                y=self.y,
                width=self.width,
                height=self.height)
    def select(self, id):
        self.active = id
        self.update()