from file import *
from tkinter import ttk
import tkinter.ttk
from pager import *
import pickle
import time
import threading
import shelve
from ftplib import *

class Application(Tk):
    def __init__(self):
        super(Application, self).__init__()
        self.geometry("720x420+300+200")
        self.title("FolderBackup")
        self.config(bg="grey")

        self.step       = 1
        self.action     = "ftp"
        self.dirlist    = []
        self.copy       = []
        self.storage    = []
        self.FTPLoginDate = {"host" : "", "login": "", "pass" : "", "folder" : ""}

        # Images
        self.img_database = PhotoImage(file="img/database.gif")
        self.img_folder = PhotoImage(file="img/folder.gif")
        self.img_folder2 = PhotoImage(file="img/folder2.gif")
        self.img_network = PhotoImage(file="img/network.gif")
        self.img_restore = PhotoImage(file="img/restore.gif")
        self.img_file = PhotoImage(file="img/file.gif")
        self.img_disk = PhotoImage(file="img/disk.gif")
        self.img_hand = PhotoImage(file="img/hand.gif")
        self.img_trash = PhotoImage(file="img/trash.gif")
        self.img_target = PhotoImage(file="img/target.gif")
        self.img_archive = PhotoImage(file="img/archive.gif")


        # Left sidebar
        self.sidebar = Frame(self,
            bg="#222222",
            width=170)
        self.sidebar.pack(side=LEFT, fill=Y)

        # Icon
        temp = Label(self.sidebar,
            image=self.img_database,
            bg="#222222")
        temp.pack()
        temp.place(x=10, y=7)
        temp = Label(self.sidebar,
            text="FolderBackup",
            bg="#222222",
            fg="#FFFFFF",
            font=16,
            justify=LEFT)
        temp.pack()
        temp.place(x=32, y=5)

        # Sidebar Menu
        bttn_local = Button(self.sidebar,
            image=self.img_folder,
            text="Local Backup",
            fg="#FFFFFF",
            bg="#222222",
            bd=0,
            compound=TOP,
            anchor=N,
            font=16,
            activebackground="#333333",
            activeforeground="#666666",
            command=self.local)
        bttn_local.pack()
        bttn_local.place(x=10, y=40, width=150)

        bttn_net = Button(self.sidebar,
            image=self.img_network,
            text="FTP Backup",
            fg="#FFFFFF",
            bg="#222222",
            bd=0,
            compound=TOP,
            anchor=N,
            font=16,
            activebackground="#333333",
            activeforeground="#666666",
            command=self.ftp)
        bttn_net.pack()
        bttn_net.place(x=10, y=140, width=150)

        bttn_local = Button(self.sidebar,
            image=self.img_restore,
            text="Restore",
            fg="#FFFFFF",
            bg="#222222",
            bd=0,
            compound=TOP,
            anchor=N,
            font=16,
            activebackground="#333333",
            activeforeground="#666666",
            command=self.restore)
        bttn_local.pack()
        bttn_local.place(x=10, y=230, width=150)

        bttn_local = Button(self.sidebar,
            image=self.img_disk,
            text="Memory",
            fg="#FFFFFF",
            bg="#222222",
            bd=0,
            compound=TOP,
            anchor=N,
            font=16,
            activebackground="#333333",
            activeforeground="#666666",
            command=self.memory)
        bttn_local.pack()
        bttn_local.place(x=10, y=330, width=150)

        # Pager
        self.pager = Pager(self)
        self.__addpages()


        self.mainloop()
    def nextstep(self):
        if self.action == "local":
            if self.step == 1:
                if len(self.tree.get(0, END)) > 0:
                    self.step = 2
                    for i in self.tree.get(0, END):
                        self.dirlist.append(i)

                    self.tree.delete(0, END)
                    self.pager.select(2)
            elif self.step == 2:
                if self.target.get() != "":
                    self.pager.select(3)
                    for i in self.dirlist:
                        s1 = Source()
                        s1.setDirectory(i)
                        s2 = Source()
                        s2.setDirectory(self.target.get())
                        backup = Copy(s1, s2, self.progresslist)
                        backup.options["type"] = self.type.get()

                        self.copy.append(backup)
                    self.step = 3

                    obj = Backup(time.ctime(time.time()))
                    obj.copy = self.dirlist
                    obj.target = self.target.get()

                    file = open("storage.data", "a")
                    file.write("START\n")

                    file.write(time.ctime(time.time())+"\n")
                    file.write(self.target.get()+"\n")
                    for i in self.dirlist:
                        file.write(i+"\n")

                    file.write("END\n")
                    file.close()

                    self.progresslist.insert(END, "Zapisano ustawienia")
                    self.startbackup()
        elif self.action == "ftp":
            if self.step == 1:
                if len(self.tree.get(0, END)) > 0:
                    self.step = 2
                    for i in self.tree.get(0, END):
                        self.dirlist.append(i)

                    self.tree.delete(0, END)
                    self.pager.select(4)
            elif self.step == 2:
                self.FTPLoginDate["host"] = self.ftp_host.get()
                self.FTPLoginDate["user"] = self.ftp_user.get()
                self.FTPLoginDate["pass"] = self.ftp_pass.get()
                self.FTPLoginDate["folder"] = self.ftp_folder.get()
                self.pager.select(3)
                self.startbackup()
        elif self.action == "memory":
            target = self.storagelist.selection()


    def startbackup(self):
        if self.archive.get() == 0 and self.action == "local":
                Local(self.copy, self.progresslist).start()
        if self.action == "local" and self.archive.get() == 1:
            Archive(self.progresslist,
                    self.archive_type,
                    self.archive_name,
                    self.target,
                    self.dirlist,
                    self.progresslist).start()
        elif self.action == "ftp":
            SendFTP(self.FTPLoginDate,
                self.progresslist,
                self.dirlist,
                self.target.get()).start()

    def askfile(self):
        directory = filedialog.askopenfilename(initialdir="\\", title="Wskaz plik")
        if directory != "":
            self.tree.insert(END, directory)
    def askfolder(self):
        directory = filedialog.askdirectory(initialdir="\\", title="Wskaz folder")
        if directory != "":
            self.tree.insert(END, directory)
    def asktarget(self):
        directory = filedialog.askdirectory(initialdir="\\", title="Wskaz folder")
        if directory != "":
            self.target.delete(0, END)
            self.target.insert(0, directory)
    def askarchive(self):
        directory = filedialog.askopenfilename(initialdir="\\",
            title="Wskaz plik",
            filetypes=[("Archiwum zip", ".zip"), ("Archiwum tar", ".tar")])
        if directory != "":
            self.restoreArchiveName.delete(0, END)
            self.restoreArchiveName.insert(0, askfile())
    def copysrctolist(self):
        dirs = self.tree.get(0, END)
        for i in dirs:
            print(i)
    def __addpages(self):
        # Home page                         ID      0
        home = Frame(self,
            bg="grey")
        self.pager.add(home)

        # Ask sources page                  ID      1
        asksrc = Frame(self,
            bg="grey")
        self.pager.add(asksrc)

        temp = Button(asksrc,
            image=self.img_file,
            text="Dodaj plik",
            fg="#FFFFFF",
            bg="#222222",
            activebackground="#333333",
            activeforeground="#666666",
            compound=LEFT,
            anchor=W,
            font=16,
            bd=0,
            command=self.askfile)
        temp.pack()
        temp.place(x=50 ,y=10, width=200)

        temp = Button(asksrc,
            image=self.img_folder2,
            text="Dodaj folder",
            fg="#FFFFFF",
            bg="#222222",
            activebackground="#333333",
            activeforeground="#666666",
            compound=LEFT,
            anchor=W,
            font=16,
            bd=0,
            command=self.askfolder)
        temp.pack()
        temp.place(x=280 ,y=10, width=200)

        self.tree = Listbox(asksrc)
        self.tree.pack()
        self.tree.place(x=15, y=80, width=500, height=280)

        scroll = Scrollbar(asksrc)
        scroll.pack()
        scroll.place(x=510, y=80, height=280)

        self.tree.config(yscrollcommand=scroll.set)
        scroll.config(command=self.tree.yview)

        temp = Button(asksrc,
            text="Dalej",
            image=self.img_hand,
            bg="#222222",
            fg="#FFFFFF",
            bd=0,
            activebackground="#333333",
            activeforeground="#666666",
            anchor=E,
            compound=RIGHT,
            font=16,
            command=self.nextstep)
        temp.pack()
        temp.place(x=448, y=370)

        temp = Button(asksrc,
            text="Usun",
            image=self.img_trash,
            bg="#222222",
            fg="#FFFFFF",
            activebackground="#333333",
            activeforeground="#666666",
            anchor=W,
            compound=LEFT,
            font=16,
            command=lambda lb=self.tree: self.tree.delete(ANCHOR))
        temp.pack()
        temp.place(x=15, y=370)

        # Config backup                     ID      2
        config = Frame(self,
            bg="grey")
        self.pager.add(config)

        self.target = Entry(config,
            font=16,
            fg="#FFFFFF",
            bg="grey",
            cursor="arrow")
        self.target.pack()
        self.target.place(x=20, y=20, width=500)

        temp = Button(config,
            image=self.img_target,
            text="Wybierz sciezke docelowa",
            bg="#222222",
            fg="#FFFFFF",
            activebackground="#333333",
            activeforeground="#666666",
            font=16,
            anchor=W,
            compound=LEFT,
            command=self.asktarget)
        temp.pack()
        temp.place(x=20, y=50)



        # Radiobuttons
        temp = Label(config,
            text="Wybierz rodzaj kopii",
            font=16,
            bg="grey",
            fg="#FFFFFF")
        temp.pack()
        temp.place(x=20, y=125)
        self.type = StringVar()
        self.type.set("full")
        temp = Radiobutton(config,
            text="Calosciowa",
            variable=self.type,
            value="full",
            bg="grey",
            font=16,
            activebackground="grey")
        temp.pack()
        temp.place(x=20, y=150)
        temp = Radiobutton(config,
            text="Przyrostowa",
            variable=self.type,
            value="incremental",
            bg="grey",
            font=16,
            activebackground="grey")
        temp.pack()
        temp.place(x=20, y=170)

        temp = Button(config,
            text="Dalej",
            image=self.img_hand,
            bg="#222222",
            fg="#FFFFFF",
            bd=0,
            activebackground="#333333",
            activeforeground="#666666",
            anchor=E,
            compound=RIGHT,
            font=16,
            command=self.nextstep)
        temp.pack()
        temp.place(x=448, y=370)

        # Archive
        temp = Label(config,
            text="Opcje archiwizacji",
            font=16,
            bg="grey",
            fg="#FFFFFF")
        temp.pack()
        temp.place(x=320, y=125)

        self.archive = IntVar()

        temp = Checkbutton(config,
            text="Archiwizuj",
            variable=self.archive,
            font=16,
            bg="grey",
            activebackground="grey")
        temp.pack()
        temp.place(x=320, y=150)

        temp = Label(config,
            font=16,
            text="Podaj nazwe archiwum",
            bg="grey",
            fg="white")
        temp.pack()
        temp.place(x=320, y=180)

        self.archive_name = Entry(config,
            font=16,
            bg="grey",
            fg="white",
            cursor="arrow")
        self.archive_name.pack()
        self.archive_name.place(x=320, y=210, width=200)


        temp = Label(config,
            text="Wybierz rodzaj archiwum",
            font=16,
            bg="grey",
            fg="white")
        temp.pack()
        temp.place(x=320, y=240)
        self.archive_type = StringVar()
        self.archive_type.set(".zip")

        temp = Radiobutton(config,
            text="ZIP",
            font=16,
            variable=self.archive_type,
            value=".zip",
            bg="grey",
            activebackground="grey")
        temp.pack()
        temp.place(x=320, y=260)
        temp = Radiobutton(config,
            text="TAR",
            font=16,
            variable=self.archive_type,
            value=".tar",
            bg="grey",
            activebackground="grey")
        temp.pack()
        temp.place(x=320, y=280)

        # Progress of backup                ID      3
        progress = Frame(self,
            bg="grey")
        self.pager.add(progress)

        temp = ttk.Progressbar(progress,
            orient="horizontal",
            value=0)
        temp.pack()
        temp.place(x=50, y=50, width=447, height=30)

        self.progresslist = Listbox(progress, font=16)
        self.progresslist.pack()
        self.progresslist.place(x=50, y=100, width=430, height=300)

        scroll2 = Scrollbar(progress)
        scroll2.pack()
        scroll2.place(x=480, y=100, height=302)

        self.progresslist.config(yscrollcommand=scroll2.set)
        scroll2.config(command=self.progresslist.yview)

        scroll3 = Scrollbar(progress, orient="horizontal")
        scroll3.pack()
        scroll3.place(x=50, y=400, width=447)

        self.progresslist.config(xscrollcommand=scroll3.set)
        scroll3.config(command=self.progresslist.xview)


        # FTP connect                       ID      4
        ftp = Frame(self,
            bg="grey")
        self.pager.add(ftp)

        temp = Label(ftp,
            text="Laczenie z serwerem FTP",
            font=16,
            bg="grey",
            fg="white")
        temp.pack()
        temp.place(x=180, y=20)

        temp = Label(ftp,
            text="Host",
            font=16,
            bg="grey",
            fg="white")
        temp.pack()
        temp.place(x=100, y=60)

        self.ftp_host = Entry(ftp,
            font=16,
            bg="grey",
            fg="white",
            cursor="arrow")
        self.ftp_host.pack()
        self.ftp_host.place(x=220, y=60, width=160)


        temp = Label(ftp,
            text="Uzytkownik",
            font=16,
            bg="grey",
            fg="white")
        temp.pack()
        temp.place(x=100, y=90)

        self.ftp_user = Entry(ftp,
            font=16,
            bg="grey",
            fg="white",
            cursor="arrow")
        self.ftp_user.pack()
        self.ftp_user.place(x=220, y=90, width=160)


        temp = Label(ftp,
            text="Haslo",
            font=16,
            bg="grey",
            fg="white")
        temp.pack()
        temp.place(x=100, y=120)

        self.ftp_pass = Entry(ftp,
            font=16,
            bg="grey",
            fg="white",
            cursor="arrow",
            show="\u2022")
        self.ftp_pass.pack()
        self.ftp_pass.place(x=220, y=120, width=160)

        temp = Label(ftp,
            text="Folder",
            font=16,
            bg="grey",
            fg="white")
        temp.pack()
        temp.place(x=100, y=150)

        self.ftp_folder = Entry(ftp,
            font=16,
            bg="grey",
            fg="white",
            cursor="arrow",
            show="\u2022")
        self.ftp_folder.pack()
        self.ftp_folder.place(x=220, y=150, width=160)

        temp = Button(ftp,
            text="Dalej",
            image=self.img_hand,
            bg="#222222",
            fg="#FFFFFF",
            bd=0,
            activebackground="#333333",
            activeforeground="#666666",
            anchor=E,
            compound=RIGHT,
            font=16,
            command=self.nextstep)
        temp.pack()
        temp.place(x=448 ,y=370)



        # Storage                              ID      5
        storage = Frame(self,
            bg="grey")
        self.pager.add(storage)

        self.storagelist = ttk.Treeview(storage)

        self.storagelist.pack()
        self.storagelist["columns"] = ("Nazwa" ,"Data")

        self.storagelist.column("Nazwa", width=330)
        self.storagelist.heading("Nazwa", text="Nazwa")

        self.storagelist.column("Data", width=100)
        self.storagelist.heading("Data", text="Data")

        self.storagelist.place(x=0, y=0, width=520, height=370)

        scroll4 = Scrollbar(storage)
        scroll4.pack()
        scroll4.place(x=520, y=0, height=370)

        self.storagelist.config(yscrollcommand=scroll4.set)
        scroll4.config(command=self.storagelist.yview)

        temp = Button(storage,
            text="Ponow backup",
            image=self.img_hand,
            bg="#222222",
            fg="#FFFFFF",
            bd=0,
            activebackground="#333333",
            activeforeground="#666666",
            anchor=E,
            compound=RIGHT,
            font=16,
            command=self.nextstep)
        temp.pack()
        temp.place(x=390 ,y=375)


        # Restore                              ID      6
        restore = Frame(self,
            bg="grey")
        self.pager.add(restore)

        self.restoreArchiveName = Entry(restore,
            font=16,
            fg="#FFFFFF",
            bg="grey",
            cursor="arrow")
        self.restoreArchiveName.pack()
        self.restoreArchiveName.place(x=20, y=20, width=500)

        temp = Button(restore,
            image=self.img_archive,
            text="Wybierz archiwum",
            bg="#222222",
            fg="#FFFFFF",
            activebackground="#333333",
            activeforeground="#666666",
            font=16,
            anchor=W,
            compound=LEFT,
            command=self.askarchive)
        temp.pack()
        temp.place(x=20, y=50)


        self.pager.select(6)
    def local(self):
        self.pager.select(1)
        self.action = "local"
        self.dirlist.clear()
        self.copy.clear()
    def ftp(self):
        self.pager.select(1)
        self.action = "ftp"
        self.dirlist.clear()
        self.copy.clear()
    def restore(self):
        self.pager.select(6)
        self.action = "restore"
        self.dirlist.clear()
        self.copy.clear()
    def memory(self):
        self.storagelist
        self.pager.select(5)
        self.action = "memory"
        self.dirlist.clear()
        self.copy.clear()

        file = open("storage.data", "r")
        BackupNum = 0
        for line in file:
            if line == "START\n":
                BackupNum+=1
        file.close()

        file = open("storage.data", "r")
        index = 0
        num = 1
        for i in range(BackupNum):
            file.readline()
            data = file.readline()
            target = str(num) + ". " + file.readline()
            num+=1

            self.storagelist.insert("", index, target, text=target)
            index+=1
            src = file.readline()
            while src != "END\n":
                self.storagelist.insert(target, index, values=(src, data))
                index+=1
                src = file.readline()

        file.close()


class Pb(threading.Thread):
    def __init__(self, pb, size, msize, dir):
        threading.Thread.__init__(self)
        self.pb = pb
        self.size = 0
        self.max_size = msize-size
        self.dir = dir
        self.pb = pb

        self.pb.configure(value=0, maximum=self.max_size)
    def run(self):
        size = getFolderSize(self.dir)-self.size
        while size<self.max_size:
            pass


class Local(threading.Thread):
    def __init__(self, filelist, entry):
        threading.Thread.__init__(self)
        self.filelist = filelist
        self.entry = entry
    def run(self):
        for i in self.filelist:
            i.MakeCopy()
        self.entry.insert(END, "Zakonczono kopiowanie")


class Archive(threading.Thread):
    def __init__(self, proglist, artype, arname, target, dirlist, entry):
        threading.Thread.__init__(self)
        self.progresslist = proglist
        self.archive_type = artype
        self.archive_name = arname
        self.target = target
        self.dirlist = dirlist
        self.entry = entry
    def run(self):
        archiwum = Archiver(self.progresslist)
        archiwum.options["type"] = self.archive_type.get()
        file = self.target.get() + "/" + self.archive_name.get()
        archiwum.options["result"] = file

        for i in self.dirlist:
            files = []
            dirs = []
            ListDir(i, files, dirs)
            for j in files:
                archiwum.addfile(j)
            archiwum.create()
        self.entry.insert(END, "Zakonczono archiwizacje")


class SendFTP(threading.Thread):
    def __init__(self, FTPData, entry, dirlist, folder):
        threading.Thread.__init__(self)
        self.FTP = FTPData
        self.entry = entry
        self.dirlist = dirlist
        self.folder = folder
    def run(self):
        ftp = FTP(self.FTP["host"],
            self.FTP["user"],
            self.FTP["pass"])

        if(self.folder != ""):
            try:
                ftp.cwd(folder)
            except:
                ftp.mkd(folder)
                ftp.cwd(folder)

        print(ftp.nlst())

        for i in self.dirlist:
            file = open(i, "r")
            head, tail = os.path.split(i)
            ftp.storlines(("STOR "+tail).translate("ascii"), file)
            file.close()
            print("Skopiowano " + i)
            self.entry.insert(END, "Wyslano "+i)

        ftp.quit()




if __name__ == "__main__":
    print("FolderBackup application class")
