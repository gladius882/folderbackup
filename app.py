from tkinter import *
from tkinter import filedialog
from file import *

class Application(Frame):
    def __init__(self, master):
        super(Application, self).__init__(master)
        self.configure(bg = "grey")
        self.grid(padx=10, pady=10)
        self.backuplist = []
        self.create_widget()

    def create_widget(self):
        self.list1 = Listbox(self,
            height=13)
        self.list1.grid(row=0, column=0, rowspan=6, sticky=N, padx=10)

        Button(self,
            text="Dodaj",
            width=20,
            command=self.add_copy).grid(row=0, column=1, sticky=N)
        Button(self,
            text="Usun",
            width=20).grid(row=1, column=1)

        Label(self,
            text="Wybierz rodzaj kopii:",
            bg="grey").grid(row=2, column=1)
        self.type = StringVar()
        self.type.set(None)
        Radiobutton(self,
            text="Kopia calosciowa",
            variable=self.type,
            value="full",
            bg="grey").grid(row=3, column=1)
        Radiobutton(self,
            text="Kopia calosiowa",
            variable=self.type,
            value="incremental",
            bg="grey").grid(row=4, column=1)

        Button(self,
            text="Wykonaj kopie",
            width=20).grid(row=5, column=1, sticky=S)

        self.log = Text(self,
            height=10,
            width=20,
            font="12").grid(row=0, column=2, rowspan=6, sticky=N, padx=10)

    def add_copy(self):
        src = Source()
        src.setDirectory(filedialog.askdirectory(title="Wybierz sciezke zrodlowa", initialdir="\\"))
        target = Source()
        target.setDirectory(filedialog.askdirectory(title="Wybierz sciezke docelowa"))

        new = Copy(src, target)
        self.backuplist.append(str(new))

        self.load_backup()
    def load_backup(self):
        for i in self.backuplist:
            self.list1.insert(END, str(i))
        self.list1.insert(END, "siema")

if __name__ == '__main__':
    print("Modul glowny aplikacji")
